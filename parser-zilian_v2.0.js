'use strict';

let request = require('request');
let _ = require('lodash');
let fs = require('fs');
let readline = require('readline');
let csv = require("fast-csv");
let he = require('he');
let MagentoExporter = require('./MagentoExporter');

let magentoExporter = new MagentoExporter("Zilian", "", "Zilian")

let inputImagesMap = "import/zilian_dumb_22JUN_with_images.csv";
let inputVariantsBarCodes = "import/zilian_barcodes_variants.csv";
let inputPrices = "import/Inv_29_junho.csv";

let outputFile = "output/import_for_magento_zilian_experimental.csv";
let outputIncompleteFile = "output/incomplete_products_experimental.csv";

// MARCA
let brand = {
    "ZA": 'Coleção de Acessórios',
    "ZF": 'Colecção de Festa',
    "ZS": 'Colecção normal'
}

// TIPOS
let division = {
    "00": "sem divisão",
    "01": "sapato fechado",
    "02": "sapato aberto",
    "03": "ténis",
    "04": "sandálias",
    "05": "botas de cano alto",
    "06": "botas de cano médio",
    "08": "botins",
    "09": "botim aberto",
    "10": "carteira",
    "13": "sabrinas"
}

// FAMILY
let family = {
    "00": "sem família",
    "01": "salto alto",
    "02": "salto médio",
    "04": "sem salto"
}

// TAMANHOS
let sizes = [
    35,
    36,
    37,
    38,
    39,
    40,
    41
];

// MATERIAIS
let materials = {
    "00": "s/mat",
    "01": "Pele",
    "02": "Pele Quebrada",
    "07": "Verniz",
    "09": "Pele Metalizada",
    "10": "Camurça",
    "14": "Cobra",
    "15": "Cobra Metalizada",
    "23": "Cetim",
    "31": "Veludo",
    "35": "Tecido",
    "45": "Pele Entrançada",
    "48": "Nubuck",
    "49": "Laminado",
    "63": "Ganga",
    "66": "Pele Gravada",
    "76": "Croute"
}

// CORES
let colors = {
    "00": "s/cor",
    "01": "Preto",
    "02": "Castanho Ruívo",
    "03": "Castanho Escuro",
    "06": "Castanho",
    "07": "Bege",
    "08": "Toupeira",
    "09": "Roxo",
    "10": "Prata",
    "11": "Verde",
    "12": "Vermelho",
    "13": "Mostarda",
    "14": "Bordeaux",
    "15": "Bronze",
    "16": "Azul",
    "17": "Cinza",
    "18": "Natural",
    "19": "Fuschia",
    "21": "Dourado",
    "22": "Salmão",
    "23": "Camel",
    "26": "Chumbo",
    "27": "Branco",
    "29": "Rosa Seco",
    "31": "Violeta",
    "32": "Marinho",
    "33": "Kaki",
    "35": "Laranja",
    "36": "Amarelo",
    "41": "Make Up",
    "43": "Blush",
    "45": "Off White",
    "46": "Rosa",
    "50": "Azul Claro",
    "54": "Coral",
    "60": "Telha",
    "64": "Verde Água Escuro",
    "71": "Multicor",
    "76": "Floral",
    "80": "Verde Água",
    "82": "Morango",
    "83": "Azul Velho",
    "84": "Ocre",
    "85": "Esmeralda"
}

// Define outputFile for Magento loader
fs.appendFileSync(outputFile, magentoExporter.getMagentoHeaderLine());

let productsMissingImages = [];
let productsMissingPrices = [];

let productsImages = {};
let productsPromoPrices = {};
let configurables = {};

let p_count = 0;

let stream1 = fs.createReadStream(inputImagesMap);

// load all product images
csv.fromStream(stream1, {headers: true}).on("data", function(line) {

    let ref = line['Referência Zilian'];
    let variant = line['Cód. Variante'];
    let baseImage = line['Base Image'] || "";
    let additionalImages = line['Other Images List'] || "";
    additionalImages = additionalImages.replace(/\s+/g, '');
    let fullRef = ref + '' + variant;

    let productImages = {
        baseImage,
        additionalImages
    }

    if (!productsImages[fullRef])
        productsImages[fullRef] = productImages;

    p_count++;

}).on("end", function() {

    // finished loading images for products
    console.log("Loaded " + p_count + " products for image map.");
    p_count = 0;

    // start processing promo prices
    let stream2 = fs.createReadStream(inputPrices);

    csv.fromStream(stream2, {headers: true}).on("data", function(line) {

        let ref = line['ref'];
        let variant = line['variant'];
        let fullRef = ref + '' + variant;
        let price = line['PVP'];
        let promoPrice = line['PVP Promo'];

        let productPromoPrice = {
            price,
            promoPrice
        }

        if (!productsPromoPrices[fullRef])
            productsPromoPrices[fullRef] = productPromoPrice;

        p_count++;

    }).on("end", function() {

        // finished loading images for products
        console.log("Loaded " + p_count + " products for promo prices.");
        p_count = 0;

        // start processing products, variants with barcodes

        let stream3 = fs.createReadStream(inputVariantsBarCodes);

        // let lc = 0;
        csv.fromStream(stream3, {headers: true}).on("data", function(line) {

            writeToMagentoLoader(line);
            p_count++;

        }).on("end", function() {

            console.log("Loaded " + p_count + " product variants.");
            p_count = 0;

            // loop all variants
            _.each(configurables, (value, key) => {

                value.configurable_variations = value.configurable_variations.substr(0, value.configurable_variations.length - 1);

                let productLine = magentoExporter.getMagentoLine("configurable", value.ref, value.category, value.name, value.price, value.promoPrice, value.description, value.baseImage, value.additional_images, value.additional_attributes, value.configurable_variations);
                fs.appendFileSync(outputFile, productLine);

                p_count++;
            });

            console.log("Created " + p_count + " product roots.");

            showFailedProducts();

            console.log("woooohh! done!");
        });

    });

});

function writeToMagentoLoader(line) {

    // console.log(line);

    let barcode = line['barcode'];
    let ref = line['ref'];

    let variant = line['variant'];

    let size = variant.substr(0, 2);
    variant = variant.substr(2);
    // console.log("new variant: " + variant);

    let fullRef = ref + '' + variant;
    // console.log("Full ref: " + fullRef);

    let productPrices = getPricesForRef(fullRef);
    // console.log("Prices for product: " + JSON.stringify(productPrices));



    // Found product ref without prices - ignoring for mangeto loader
    if(!productPrices){

        productsMissingPrices.push({ fullRef });

    } else {

        let price = productPrices.price;
        let promoPrice = productPrices.promoPrice;

        let description = "";

        let category = getDivision(ref);

        let name = getVariantName(category, variant);

        let brand = getBrand(ref);

        let additional_attributes = "";

        if (getFamily(ref) != 'sem família')
            additional_attributes += `heel_size=${getFamily(ref)},`;

        additional_attributes = additional_attributes.substr(0, additional_attributes.length - 1);

        let productImages = getImagesForRef(fullRef);



        // Found product ref without images - ignoring for magento loader
        if(!productImages){

            productsMissingImages.push({ fullRef });

        } else {

            let baseImage = productImages.baseImage;

            let additional_images = productImages.additionalImages;

            if (!configurables[ref]) {
                configurables[ref] = {
                    barcode,
                    ref,
                    category,
                    description,
                    price,
                    promoPrice,
                    brand,
                    additional_attributes,
                    baseImage,
                    additional_images,
                    name: getProductBaseName(category, ref),
                    configurable_variations: ""
                }
            }
            additional_attributes += `,barcode=${barcode}`

            // additional_attributes
            // configurable_variations
            let additional_attributes2 = ""

            let sku = `${ref}_${size}${variant}`;
            additional_attributes2 += `size=${size},`;
            let pm = getPrimaryMaterial(variant);
            let pc = getPrimaryColor(variant);
            let sm = getSecondaryMaterial(variant);
            let sc = getSecondaryColor(variant);


            if (pm != 's/mat')
                additional_attributes2 += `material=${pm},`;
            if (sm != 's/mat')
                additional_attributes2 += `secondary_material=${sm},`;
            if (sc != 's/cor')
                additional_attributes2 += `secondary_color=${sc},`;
            if (pc != 's/cor')
                additional_attributes2 += `color=${pc},`;



            description = `${pm} ${pc} ${sm} ${sc}`;

            configurables[ref].description = description;

            additional_attributes2 = additional_attributes2.substr(0, additional_attributes2.length - 1);
            configurables[ref].configurable_variations += `sku=${sku},${additional_attributes2}|`;

            additional_attributes2 = `${additional_attributes?additional_attributes+',':''}${additional_attributes2}`



            let productLine = magentoExporter.getMagentoLine("simple", sku, category, name, price, promoPrice, description, baseImage, additional_images, additional_attributes2, "");
            fs.appendFileSync(outputFile, productLine);

        }


    }


}



// HELPERS  -----------------------------------------------

function getBrand(ref) {
    return brand[ref.substr(0, 2)];
}

function getCollection(ref) {
    return ref.substr(2, 3);
}

function getFamily(ref) {
    return family[ref.substr(5, 2)];
}

function getDivision(ref) {
    return division[ref.substr(7, 2)];
}

function getRefCode(ref) {
    return ref.substr(11, 4);
}

function getPrimaryMaterial(variant) {
    return materials[variant.substr(0, 2)];
}

function getPrimaryColor(variant) {
    return colors[variant.substr(2, 2)];
}

function getSecondaryMaterial(variant) {
    return materials[variant.substr(4, 2)];
}

function getSecondaryColor(variant) {
    return colors[variant.substr(6, 2)];
}

function getVariantName(category, variant) {
    let pm = getPrimaryMaterial(variant);
    let pc = getPrimaryColor(variant);
    let sm = getSecondaryMaterial(variant);
    let sc = getSecondaryColor(variant);

    let name = category;

    if (pm != 's/mat')
        name += ` ${pm}`;
    if (pc != 's/cor')
        name += ` ${pc}`;
    if (sm != 's/mat')
        name += ` ${sm}`;
    if (sc != 's/cor')
        name += ` ${sc}`;

    return name;
}

function getProductBaseName(category, ref) {
    let refCode = getRefCode(ref);
    let name = category;

    if (refCode)
        name += ` ${refCode}`;

    return name;
}

function getImagesForRef(fullRef) {
    return productsImages[fullRef];
}

function getPricesForRef(fullRef) {
    return productsPromoPrices[fullRef];
}

function showFailedProducts() {

    let productsWithDefect = "FAILED NO PRICE\n";

    // console.log("-------------------------------------------------------------------------");
    _.each(productsMissingPrices, p => {
        // console.log(p.fullRef);
        productsWithDefect += `${p.fullRef}\n`;
    });


    productsWithDefect += "-------------------------\n";
    productsWithDefect += "FAILED NO IMAGES\n";
    // console.log("-------------------------------------------------------------------------");
    _.each(productsMissingImages, p => {
        // console.log(p.fullRef);
        productsWithDefect += `${p.fullRef}\n`;
    });


    console.log(" ----------------------------------------");
    console.log("Products without PRICES: " + productsMissingPrices.length);
    console.log("Products without IMAGES: " + productsMissingImages.length);
    console.log(" ----------------------------------------");

    fs.appendFileSync(outputIncompleteFile, productsWithDefect);
}
