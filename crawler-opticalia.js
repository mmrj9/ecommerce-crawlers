'use strict';

const request = require('request');
const cheerio = require('cheerio');
const URL = require('url-parse');
const _ = require('lodash');
const fs = require('fs');
const readline = require('readline');
const he = require('he');
const MagentoExporter = require('./MagentoExporter');

const BASE_URL = "https://www.framesdirect.com";

// const MAX_PAGES_TO_VISIT = 105;
const MAX_PAGES_TO_VISIT = 3;
let MAX_PRODUCTS_TO_VISIT = 0;

let numPagesVisited = 0;
let OUTPUT_CSV_FILE = "output/importOpticalia.csv";

const MAGENTOEXPORTER = new MagentoExporter("Opticalia", "default", "Default");

let pagesToVisit = _.map(
  _.rangeRight(MAX_PAGES_TO_VISIT),
  pageNumber => `${BASE_URL}/women/prescription-eyeglasses.aspx?department=Eyeglasses&gender=Women&p=${pageNumber + 1}&rows=25&sortby=1&catalogsubtype=0&camefrom=/women/prescription-eyeglasses.aspx`
);
// pagesToVisit = [`${BASE_URL}`];

console.log(pagesToVisit)

let PRODUCTS_TO_VISIT = []

// STEP 1 ____________________ Start crawling
console.log('start crawling ...');
getListOfProductDetailsLinks(function() {
  fs.writeFile(OUTPUT_CSV_FILE, '', function() {});
  fs.appendFileSync(OUTPUT_CSV_FILE, MAGENTOEXPORTER.getMagentoHeaderLine());

  startCrawlingProductsDetails()
});
function startCrawlingProductsDetails() {
  MAX_PRODUCTS_TO_VISIT = PRODUCTS_TO_VISIT.length;
  console.log(PRODUCTS_TO_VISIT.length)
  crawlProductLinks();
}

function getListOfProductDetailsLinks(callback) {
  if (numPagesVisited >= 1) {
    console.log("Reached max limit of number of pages to visit.");
    callback()
    return;
  }
  var nextPage = pagesToVisit.pop();

  visitPage(nextPage, getListOfProductDetailsLinks, callback);
}

// Get Product Links
function visitPage(url, callback, callback2) {

  console.log("Visiting page " + url);
  numPagesVisited++;

  //request(`${baseURL}/en/shop/page/${i}/`, function(error, response, body) {
  request(`${url}`, function(error, response, body) {

    // Check status code (200 is HTTP OK)
    console.log("Status code: " + response.statusCode);
    if (error || response.statusCode !== 200) {
      callback(callback2);
      return;
    }

    // Parse the document body
    let $ = cheerio.load(body, {decodeEntities: false});

    // get absolute url for each product in current page
    let aaas = $(".prod-model");
    aaas.each(function(i, elem) {
      let link = `${$(this).attr('href')}`;

      if (!_.includes(PRODUCTS_TO_VISIT, link)) {
        PRODUCTS_TO_VISIT.push(`${BASE_URL}${link}`);
      }
    });

    console.log(PRODUCTS_TO_VISIT);
    callback(callback2);

  });
}

function crawlProductLinks() {
  if (numPagesVisited >= MAX_PRODUCTS_TO_VISIT) {
    console.log("Reached max limit of products to visit.");
    return;
  }
  var nextPage = PRODUCTS_TO_VISIT.pop();

  visitProductDetails(nextPage, crawlProductLinks);
}

// Get Product Details
function visitProductDetails(url, callback) {

  console.log("Visiting page details " + url);
  numPagesVisited++;

  // get product details page
  request(url, function(error, response, body) {

    // Check status code (200 is HTTP OK)
    if (error || response.statusCode !== 200) {
      console.log("ERROR");
      console.log(error);
      // setTimeout(callback, 1000);
      return;
    }

    // console.log(body);
    let $ = cheerio.load(body, {decodeEntities: false});

    let additional_images = "";
    let additional_attributes = "";
    let related_products = "";

    let baseImage = $("meta[property='og:image']").attr("content");
    if (baseImage) {
      baseImage = baseImage.trim();
      baseImage = baseImage.replace("sr8.jpg", "sr9.jpg")
    } else {
      baseImage = "";
    }

    let allImagesLinks = [baseImage];

    let sku = $("span[itemprop='productID']").text().trim();
    if (sku) {
      sku = he.decode(sku)
    }

    baseImage = `${sku}-0.jpg`;

    let name = $("[itemprop='model']").text().trim();
    if (name) {
      name = he.decode(name)
    }

    let auxPrice = $("[itemprop='total-retail']").text();
    let price = 100;
    if (!_.isEmpty(auxPrice)) {
      price = $("[itemprop='total-retail']").text().split(" ")[1].trim();
    }

    if (price) {
      price = parseFloat(price)
    }

    let description = $("[itemprop='description']").text().trim().replace(/;/g, "").replace(/[\u2018\u2019]/g, "'");
    if (description) {
      description = he.decode(description)
    }
    let category = $("h1 > span").text().trim();
    if (category) {
      category = he.decode(category)
    }

    let brandAux = $("[itemprop='name']");
    let brand = "";
    if (Array.isArray(brandAux)) {
      brand = $("[itemprop='name']").pop().text().trim();
    } else {
      brand = $("[itemprop='name']").text().trim();
    }
    if (brand) {
      brand = he.decode(brand)
    }

    sku = MAGENTOEXPORTER.cleanSku(sku);

    if (!baseImage || !category || !price || !sku) {
      callback()
      return
    }

    console.log(`SKU ${sku}`)
    console.log(`NAME ${name}`)
    console.log(`DESCRIPTION ${description}`)
    console.log(`PRICE ${price}`)
    console.log(`CATEGORY ${category}`)
    console.log(`IMAGE ${baseImage}`)
    console.log(`BRAND ${brand}`)

    let productLine = MAGENTOEXPORTER.getMagentoLine(
      "simple",
      "Not Visible Individually",
      sku,
      category,
      name,
      price,
      "",
      description,
      baseImage,
      baseImage,
      additional_images,
      additional_attributes,
      "",
      related_products
    );
    fs.appendFileSync(OUTPUT_CSV_FILE, productLine);

    _.each(allImagesLinks, function(imgSrc, index) {
      download(imgSrc, `./images/opticalia/${sku}-${index}.jpg`, function(success) {
        if (success) {} else {
          console.log("Cannot get image for product " + name);
        }
      });
    })

    callback();

  });
}

// Download Images
let download = function(uri, filename, callback) {
  request.head(uri, function(err, res, body) {
    if (res && res.headers['content-length'] != "0") {
      request(uri).pipe(fs.createWriteStream(filename)).on('close', function() {
        callback(true);
      });
    } else {
      callback(false);
    }
  });
};
