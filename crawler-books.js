'use strict';

const request = require('request');
const cheerio = require('cheerio');
const URL = require('url-parse');
const _ = require('lodash');
const fs = require('fs');
const readline = require('readline');
const he = require('he');
const MagentoExporter = require('./MagentoExporter');
const csv = require("fast-csv");


//constructor(rootCategory, storeViewCode, attributeSet) {
const magentoExporter = new MagentoExporter("Books", "books", "Default")

fs.writeFile('output/booksToImport.csv', '', function(){});
fs.appendFileSync('output/booksToImport.csv', magentoExporter.getMagentoHeaderLine());

csv
 .fromPath("import/books - 100.csv", {delimiter : ";"})
 .on("data", function(data){
     let sku = data[0];
     let date = data[1];
     let company = data[2];
     let author = data[3];
     let name = data[4];

     let line = magentoExporter.getMagentoLine(sku, "Books", name, "0.0", name, `${sku}.jpg`, author);
     fs.appendFileSync('output/booksToImport.csv', line);
 })
 .on("end", function(){
     console.log("done");
 });
