'use strict';

const request = require('request');
const cheerio = require('cheerio');
const URL = require('url-parse');
const _ = require('lodash');
const fs = require('fs');
const readline = require('readline');
const he = require('he');
const MagentoExporter = require('./MagentoExporter');

const BASE_URL = "http://zilian.com";

// const MAX_PAGES_TO_VISIT = 105;
const MAX_PAGES_TO_VISIT = 15;
let MAX_PRODUCTS_TO_VISIT = 0;

let numPagesVisited = 0;
let productDetailsLinks = [];


const magentoExporter = new MagentoExporter("Zilian", "zilian", "Default")

let pagesToVisit = _.map(_.rangeRight(MAX_PAGES_TO_VISIT), pageNumber => `${BASE_URL}/loja-online/spring-summer-17?page=${pageNumber + 1}`);

// STEP 1 ____________________
// Start crawling
// console.log('start crawling ...');
// crawl();
// fs.writeFile('import/zilian-page-links.csv', '', function(){});
// END OF STEP 1 ____________________


// STEP 2 ____________________

console.log('start reading productLinks file ...');
let lineReader = readline.createInterface({
  input: require('fs').createReadStream('import/zilian-page-links.csv')
});

// fs.writeFile('output/importTorres.csv', '', function(){});
fs.appendFileSync('output/importZilian.csv', magentoExporter.getMagentoHeaderLine());

lineReader.on('line', function (line) {
  // console.log('Line from file:', line);
  productDetailsLinks.push(line);
  // console.log(productDetailsLinks.length);
});

// star product details crawling after all links have been loaded
lineReader.on('close', startCrawlingProductsDetails);

// END OF STEP 2 ____________________

function startCrawlingProductsDetails() {
    MAX_PRODUCTS_TO_VISIT = productDetailsLinks.length;
    crawlProductLinks();
}


function crawl() {
    if (numPagesVisited >= MAX_PAGES_TO_VISIT) {
        console.log("Reached max limit of number of pages to visit.");
        return;
    }
    var nextPage = pagesToVisit.pop();

    visitPage(nextPage, crawl);
}


// Get Product Links
function visitPage(url, callback) {

    console.log("Visiting page " + url);
    numPagesVisited++;

    //request(`${baseURL}/en/shop/page/${i}/`, function(error, response, body) {
    request(url, function(error, response, body) {

        // Check status code (200 is HTTP OK)
        console.log("Status code: " + response.statusCode);
        if (error || response.statusCode !== 200) {
            callback();
            return;
        }

        // Parse the document body
        let $ = cheerio.load(body, { decodeEntities: false });

        // get absolute url for each product in current page
        $("#product_list").children("li").children("a").each(function(i, elem) {
            let link = `${BASE_URL}${$(this).attr('href')}`;
            // console.log(link);
            // fs.appendFileSync('import/torres-page-links.csv', `${link}\n`);

            if(!_.includes(productDetailsLinks, link)){
                productDetailsLinks.push(link);
                fs.appendFileSync('import/zilian-page-links.csv', `${link}\n`);
            }
        });

        callback();

    });
}


function crawlProductLinks() {
    if (numPagesVisited >= MAX_PRODUCTS_TO_VISIT) {
        console.log("Reached max limit of products to visit.");
        return;
    }
    var nextPage = productDetailsLinks.pop();

    visitProductDetails(nextPage, crawlProductLinks);
}

// Get Product Details
function visitProductDetails(url, callback) {

    console.log("Visiting page details " + url);
    numPagesVisited++;

    // get product details page
    request(url, function(error, response, body) {

        // Check status code (200 is HTTP OK)
        if (error || response.statusCode !== 200) {
            console.log("ERROR");
            console.log(error);
            // setTimeout(callback, 1000);
            return;
        }

        // console.log(body);
        let $ = cheerio.load(body, { decodeEntities: false });

        // TODO - blindar contra nulls


        let image = `${BASE_URL}${$($('a.thumb')[0]).attr('href')}`;
        // console.log(image);

        let sku = $("#content > div > div.article > div > div > div.text > p.sub-title").text().trim();
        if (sku) {
            sku = he.decode(sku)
        }

        let name =$('#content > div > div.article > div > div > div.text > h1').text().trim();
        if (name) {
            name = he.decode(name)
        }

        let price = $("#content > div > div.article > div > div > div.text > p.price > strong").text().trim();
        if (price) {
            price = he.decode(price.substr(price, price.length - 1))
        }
        let description = $('#content > div > div.article > div > div > div.text > div.description').html();
        if (description) {
            description = he.decode(description)
        }
        let category = name.split(' ')[0];
        if (category) {
            category = he.decode(category)
        }
        let brand = 'Zilian';
        if (brand) {
            brand = he.decode(brand)
        }

        sku = magentoExporter.cleanSku(sku);

        // writing product details do file
        if (price !== '0') {
            console.log(`Image : ${image}`);
            download(image, `./images/zilian/${sku}.jpg`, function(success) {
                if (success) {
                    let productLine = magentoExporter.getMagentoLine(sku, category, name, price, JSON.stringify(description), `${sku}.jpg`, brand);
                    fs.appendFileSync('output/importZilian.csv', productLine);
                }else {
                    console.log("Cannot get image for product " + name);
                }
            });
        }

        callback();

    });
}

// Download Images
let download = function(uri, filename, callback) {
    request.head(uri, function(err, res, body) {
        if (res && res.headers['content-length'] != "0") {
            request(uri).pipe(fs.createWriteStream(filename)).on('close', function(){
                callback(true);
            });
        }else {
            callback(false);
        }
    });
};
