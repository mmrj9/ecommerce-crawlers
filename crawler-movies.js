'use strict';

const request = require('request');
const cheerio = require('cheerio');
const URL = require('url-parse');
const _ = require('lodash');
const fs = require('fs');
const readline = require('readline');
const he = require('he');
const MagentoExporter = require('./MagentoExporter');
const csv = require("fast-csv");

const magentoExporter = new MagentoExporter("Movies", "movies", "Default")

fs.writeFile('output/moviesToImport.csv', '', function(){});
fs.appendFileSync('output/moviesToImport.csv', magentoExporter.getMagentoHeaderLine());

csv
 .fromPath("import/movies - 100.csv")
 .on("data", function(data){
     let sku = data[0];
     let name = data[1];
     let category = data[2];

     name = name.substring(0, name.indexOf("(") - 1);
     category = category.replace(/\|/g, '/');

     let url = `http://www.omdbapi.com/?t=${name}&plot=short&r=json`
     setTimeout(function(){
         request(url, function(error, response, body) {

             if (response && response.statusCode == 200) {
                let body = JSON.parse(response.body);


                if (body.Title){

                      download(body.Poster, `./images/movies/${sku}.jpg`, function(success) {

                          if(success){
                              let line = magentoExporter.getMagentoLine(sku, category, body.Title, "0.0", body.Plot, `${sku}.jpg`, body.Director);
                              fs.appendFileSync('output/moviesToImport.csv', line);
                          }
                      });

                }else {
                    console.log(data);
                    // console.log(`Cannot find details for ${name}:  ${JSON.stringify(body)}`);
                }
            }
         })
     }, 1000)
 })
 .on("end", function(){
     console.log("done");
 });

// Download Images
let download = function(uri, filename, callback) {
    request.head(uri, function(err, res, body) {
        if (res && res.headers['content-length'] != "0") {
            request(uri).pipe(fs.createWriteStream(filename)).on('close', function(){
                callback(true);
            });
        }else {
            callback(false);
        }
    });
};
