'use strict';

let request = require('request');
let _ = require('lodash');
let fs = require('fs');
let readline = require('readline');
let csv = require("fast-csv");
let MagentoExporter = require('./MagentoExporter');

let magentoExporter = new MagentoExporter("Zilian", "", "Zilian")

let imagesPath = '/Users/dmsilva/Desktop/zs_final_images/zs_final';
let inputVariantsBarCodes = "import/zilian_barcodes_variants_for_dev_tests.csv";
let inputPrices = "import/Inv_29_junho.csv";

// default image for products that dont have one
let noImageFileName = "no_image.jpg";

let outputFilePrefix = "output/import_for_magento_zilian";
let outputIncompleteFile = "output/incomplete_products_experimental.csv";

// MARCA
let brand = {
    "ZA": 'Coleção de Acessórios',
    "ZF": 'Colecção de Festa',
    "ZS": 'Colecção normal'
}

// TIPOS
let division = {
    "00": "sem divisão",
    "01": "sapato fechado",
    "02": "sapato aberto",
    "03": "ténis",
    "04": "sandálias",
    "05": "botas de cano alto",
    "06": "botas de cano médio",
    "08": "botins",
    "09": "botim aberto",
    "10": "carteira",
    "13": "sabrinas"
}

// FAMILY
let family = {
    "00": "sem família",
    "01": "salto alto",
    "02": "salto médio",
    "04": "sem salto"
}

// TAMANHOS
let sizes = [
    35,
    36,
    37,
    38,
    39,
    40,
    41
];

// MATERIAIS
let materials = {
    "00": "s/mat",
    "01": "Pele",
    "02": "Pele Quebrada",
    "07": "Verniz",
    "09": "Pele Metalizada",
    "10": "Camurça",
    "14": "Cobra",
    "15": "Cobra Metalizada",
    "23": "Cetim",
    "31": "Veludo",
    "35": "Tecido",
    "45": "Pele Entrançada",
    "48": "Nubuck",
    "49": "Laminado",
    "63": "Ganga",
    "66": "Pele Gravada",
    "76": "Croute"
}

// CORES
let colors = {
    "00": "s/cor",
    "01": "Preto",
    "02": "Castanho Ruívo",
    "03": "Castanho Escuro",
    "04": "Caramelo",
    "05": "Castanho Claro",
    "06": "Castanho",
    "07": "Bege",
    "08": "Toupeira",
    "09": "Roxo",
    "10": "Prata",
    "11": "Verde",
    "12": "Vermelho",
    "13": "Mostarda",
    "14": "Bordeaux",
    "15": "Bronze",
    "16": "Azul",
    "17": "Cinza",
    "18": "Natural",
    "19": "Fuschia",
    "20": "Cinzento Claro",
    "21": "Dourado",
    "22": "Salmão",
    "23": "Camel",
    "24": "Pinho",
    "25": "Maçã",
    "26": "Chumbo",
    "27": "Branco",
    "28": "Gelo",
    "29": "Rosa Seco",
    "30": "Lilás",
    "31": "Violeta",
    "32": "Marinho",
    "33": "Kaki",
    "34": "Beringela",
    "35": "Laranja",
    "36": "Amarelo",
    "37": "Turquesa",
    "38": "Azul Escuro",
    "39": "Verde Azeitona",
    "40": "Cinzento Escuro",
    "41": "Make Up",
    "42": "Cereja",
    "43": "Blush",
    "44": "Salvia",
    "45": "Off White",
    "46": "Rosa",
    "47": "Fogo",
    "48": "Amarelo Canário",
    "49": "Areia",
    "50": "Azul Claro",
    "51": "Azul Petróleo",
    "52": "Bege Claro",
    "53": "Bege Escuro",
    "54": "Coral",
    "55": "Marfim",
    "56": "Ouro",
    "57": "Tropical",
    "58": "Prata Velha",
    "59": "Rosa Vintage",
    "60": "Telha",
    "61": "Turquesa Claro",
    "62": "Turquesa Escuro",
    "63": "Verde Agua Claro",
    "64": "Verde Água Escuro",
    "65": "Verde Escuro",
    "66": "Verde Lima",
    "67": "Verde Maçã",
    "68": "Verde Pinho",
    "69": "Verde Seco",
    "70": "Champanhe",
    "71": "Multicor",
    "72": "Pêssego",
    "73": "Leopardo",
    "74": "Jaguar",
    "75": "Zebra",
    "76": "Floral",
    "77": "Tartan",
    "78": "Cobra",
    "79": "Toupeira Escuro",
    "80": "Verde Água",
    "81": "Transparente",
    "82": "Morango",
    "83": "Azul Velho",
    "84": "Ocre",
    "85": "Esmeralda",
    "86": "Dálmata"
}

let allLinesToInsertInExcel = {};
let refsCount = {};

// Define outputFile for Magento loader


let productsMissingImages = [];
let productsMissingPrices = [];

let productsImages = {};
let productsPromoPrices = {};
let configurables = {};

let p_count = 0;

// load all images in path
fs.readdir(imagesPath, function(err, items) {

    console.log("entering images folder: " + imagesPath);

    let fileName = "";
    let ref = "";
    let variant = "";
    let fullRef = "";
    let baseImage = "";
    let additionalImages = [];



    // loop all files in directory
    for (var i = 0; i < items.length; i++) {
        // console.log(items[i]);
        // console.log(items.length);

        fileName = items[i];

        if(fileName[0] == 'Z'){

            // get variant
            let fileNameComponents = fileName.split('_');


            // get reference
            ref = fileNameComponents[0];

            if (fileNameComponents[fileNameComponents.length - 1].trim() != "small.jpg") {

                console.log("file name components: " + fileNameComponents);
                // base image scenario
                if(fileNameComponents.length == 2){
                    variant = fileNameComponents[1].split('.')[0];
                    fullRef = ref + '' + variant;

                    // console.log(`ref: ${ref}  variant: ${variant}`);

                    if (!productsImages[fullRef]){
                        productsImages[fullRef] = { baseImage: '', additionalImages:'' };
                    }
                    productsImages[fullRef].baseImage = fileName;
                }
                // variant images scenario
                else{
                    variant = fileNameComponents[1];
                    fullRef = ref + '' + variant;

                    // console.log(`ref: ${ref}  variant: ${variant}`);

                    if (!productsImages[fullRef]){
                        productsImages[fullRef] = { additionalImages:'' };
                    }

                    productsImages[fullRef].additionalImages += fileName + ',';
                }

                p_count++;
            }else {


            }

        }


    }


    // finished loading images for products
    console.log("Processed " + p_count + " images!");

    // console.log("Product Images: " + JSON.stringify(productsImages));

    p_count = 0;

    // DEV ABORT
    // process.exit(1);



    // start processing promo prices
    let stream2 = fs.createReadStream(inputPrices);

    csv.fromStream(stream2, {headers: true}).on("data", function(line) {

        let ref = line['ref'];
        let variant = line['variant'];
        let fullRef = ref + '' + variant;
        let price = line['PVP'];
        let promoPrice = line['PVP Promo'];

        let productPromoPrice = {
            price,
            promoPrice
        }

        if (!productsPromoPrices[fullRef])
            productsPromoPrices[fullRef] = productPromoPrice;

        p_count++;

    }).on("end", function() {

        // finished loading images for products
        console.log("Loaded " + p_count + " products for promo prices.");
        p_count = 0;

        // start processing products, variants with barcodes

        let stream3 = fs.createReadStream(inputVariantsBarCodes);

        // let lc = 0;
        csv.fromStream(stream3, {headers: true}).on("data", function(line) {

            writeToMagentoLoader(line);
            p_count++;

        }).on("end", function() {
            //will write all simple in excel

            // console.log(allLinesToInsertInExcel);
            // console.log(refsCount);
            let parents= 0;
            _.each(refsCount, function(a){
              if (a.counter > 1) {
                parents ++;
              }
            })


            // fs.appendFileSync(outputFile, magentoExporter.getMagentoHeaderLine());
            console.log("Parents : " + parents);
            console.log("Childs : " + _.size(allLinesToInsertInExcel));
            let childsKeys = _.keys(allLinesToInsertInExcel)

            var files = [];
            var getNewFile = function(){
                let fileName = outputFilePrefix + "_" + (files.length + 1) + ".csv";
                files.push(fileName);
                fs.appendFileSync(fileName, magentoExporter.getMagentoHeaderLine());
                return fileName
            }


            let fileName = ""
            _.each(childsKeys, (key, index) => {
                let productLine = ""

                if (index % 3000 == 0) {
                    fileName = getNewFile()
                }

                let value = allLinesToInsertInExcel[key];
                if (refsCount[value.ref] && refsCount[value.ref].counter > 1 ) {
                    if (value.baseImage && value.baseImage == noImageFileName) {
                        value.baseImage = "";
                      }
                    productLine = magentoExporter.getMagentoLine("simple", "Not Visible Individually", value.sku, value.category, value.name, value.price, value.promoPrice, value.description, value.baseImage, value.baseImage, value.additional_images, value.additional_attributes, "");
                }else {
                  //no parent
                  if (!value.baseImage || value.baseImage == "") {
                    value.baseImage = noImageFileName;
                  }
                  productLine = magentoExporter.getMagentoLine("simple", "Catalog, Search", value.sku, value.category, value.name, value.price, value.promoPrice, value.description, value.baseImage, value.baseImage, value.additional_images, value.additional_attributes, "");
                }
                // console.log(productLine);

                fs.appendFileSync(fileName, productLine, 'utf8');
            });

            fileName = getNewFile()
            _.each(refsCount, (value, key) => {
              if (value.counter > 1) {
                  let product = value.product;
                  product.configurable_variations = product.configurable_variations.substr(0, product.configurable_variations.length - 1);

                  if (!product.baseImage || product.baseImage == "") {
                    product.baseImage = noImageFileName;
                  }

                  let productLine = magentoExporter.getMagentoLine("configurable", "Catalog, Search", product.ref, product.category, product.name, product.price, product.promoPrice, product.description, product.baseImage, product.baseImage , product.additional_images, product.additional_attributes, product.configurable_variations);
                  fs.appendFileSync(fileName, productLine, 'utf8');

                  p_count++;
              }
            });



            console.log("Created " + p_count + " product roots.");
            console.log("output files " +files)
            // showFailedProducts();

            console.log("woooohh! done!");
        });
    });
});


function writeToMagentoLoader(line) {

    // console.log(line);

    let barcode = line['barcode'];
    let ref = line['ref'];

    let variant = line['variant'];

    let size = variant.substr(0, 2);
    variant = variant.substr(2);
    // console.log("new variant: " + variant);

    let fullRef = ref + '' + variant;
    // console.log("Full ref: " + fullRef);

    let productPrices = getPricesForRef(fullRef);
    let price = line['PVP'];
    if(price == '' || price == undefined) price = '0';
    let promoPrice = '';


    if (productPrices) {
        // price = productPrices.price
        promoPrice = productPrices.promoPrice;
    }


    let description = "";

    let category = getDivision(ref);

    let name = getVariantName(category, variant);

    let brand = getBrand(ref);

    let additional_attributes = "";

    if (getFamily(ref) != 'sem família')
        additional_attributes += `heel_size=${getFamily(ref)},`;

    additional_attributes = additional_attributes.substr(0, additional_attributes.length - 1);

    let productImages = getImagesForRef(fullRef);
    let baseImage = noImageFileName;
    let additional_images = '';

    if (productImages) {
        baseImage = productImages.baseImage;
        additional_images = productImages.additionalImages;
        additional_images = additional_images.substr(0, additional_images.length - 1);

        let additional_images_array = additional_images.split(",");

        if((!baseImage || baseImage.trim() == '') && additional_images_array.length == 0) {

        }
        else if ((!baseImage || baseImage.trim() == '') && additional_images_array.length > 0) {
          baseImage = additional_images_array[0];
        }

    }

    // let baseImageComponents = baseImage.split('.');
    // let small_image = `${baseImageComponents[0]}_small.${baseImageComponents[1]}`;
    let small_image = "";


    // DEBUG ONLY
    // console.log(fullRef + " - - - - " + baseImage + " - - - - " + additional_images);
    // fs.appendFileSync(outputIncompleteFile, fullRef + "," + baseImage + "," + additional_images + "\n");

    if (refsCount[ref]) {
      refsCount[ref].counter = refsCount[ref].counter + 1;

      if(!refsCount[ref].product.baseImage || refsCount[ref].product.baseImage == noImageFileName){
        refsCount[ref].product.baseImage = baseImage;
        refsCount[ref].product.small_image = small_image;
      }

    }else {
      refsCount[ref] = {
        counter : 1,
        product : {
          ref,
          category,
          description: "",
          price,
          promoPrice,
          brand,
          additional_attributes,
          baseImage,
          small_image,
          additional_images,
          name: getProductBaseName(category, ref),
          configurable_variations: ""
        }
      };
    }
    console.log(baseImage)

    additional_attributes += `,barcode=${barcode}`
    // additional_attributes
    // configurable_variations
    let additional_attributes2 = '';

    let sku = `${ref}_${size}${variant}`;
    additional_attributes2 = `size=${size},`;
    let pm = getPrimaryMaterial(variant);
    let pc = getPrimaryColor(variant);
    let sm = getSecondaryMaterial(variant);
    let sc = getSecondaryColor(variant);

    if (pm != 's/mat')
        additional_attributes2 += `material=${pm},`;
    if (sm != 's/mat')
        additional_attributes2 += `secondary_material=${sm},`;
    if (sc != 's/cor')
        additional_attributes2 += `secondary_color=${sc},`;
    if (pc != 's/cor')
        additional_attributes2 += `color=${pc},`;

    description = `${getProductBaseName(category, ref)} - ${pm} ${pc} ${sm} ${sc}`;
    additional_attributes2 = additional_attributes2.substr(0, additional_attributes2.length - 1);

    // console.log(JSON.stringify(refsCount));
    refsCount[ref].product.description = description;
    refsCount[ref].product.configurable_variations += `sku=${sku},${additional_attributes2}|`;

    additional_attributes2 = `${additional_attributes
        ? additional_attributes + ','
        : ''}${additional_attributes2}`;


    allLinesToInsertInExcel[sku] = {
      isSimple : true,
      ref,
      sku,
      category,
      name,
      price,
      promoPrice,
      description,
      baseImage,
      small_image,
      additional_images,
      additional_attributes: additional_attributes2,
      configurable_variations : ""
    }

    //
    // let productLine = magentoExporter.getMagentoLine("simple", sku, category, name, price, promoPrice, description, baseImage, additional_images, additional_attributes2, "");
    // fs.appendFileSync(outputFile, productLine, 'utf8');

}




// HELPERS  -----------------------------------------------

function getBrand(ref) {
    return brand[ref.substr(0, 2)];
}

function getCollection(ref) {
    return ref.substr(2, 3);
}

function getFamily(ref) {
    return family[ref.substr(5, 2)];
}

function getDivision(ref) {
    return division[ref.substr(7, 2)];
}

function getRefCode(ref) {
    return ref.substr(11, 4);
}

function getPrimaryMaterial(variant) {
    return materials[variant.substr(0, 2)];
}

function getPrimaryColor(variant) {
    return colors[variant.substr(2, 2)];
}

function getSecondaryMaterial(variant) {
    return materials[variant.substr(4, 2)];
}

function getSecondaryColor(variant) {
    return colors[variant.substr(6, 2)];
}

function getVariantName(category, variant) {
    let pm = getPrimaryMaterial(variant);
    let pc = getPrimaryColor(variant);
    let sm = getSecondaryMaterial(variant);
    let sc = getSecondaryColor(variant);

    let name = category;

    if (pm != 's/mat')
        name += ` ${pm}`;
    if (pc != 's/cor')
        name += ` ${pc}`;
    if (sm != 's/mat')
        name += ` ${sm}`;
    if (sc != 's/cor')
        name += ` ${sc}`;

    return name;
}

function getProductBaseName(category, ref) {
    let refCode = getRefCode(ref);
    let name = refCode;

    if (category)
        name += ` ${category}`;

    return name;
}

function getImagesForRef(fullRef) {
    return productsImages[fullRef];
}

function getPricesForRef(fullRef) {
    return productsPromoPrices[fullRef];
}

function showFailedProducts() {

    let productsWithDefect = "FAILED NO PRICE\n";

    // console.log("-------------------------------------------------------------------------");
    _.each(productsMissingPrices, p => {
        // console.log(p.fullRef);
        productsWithDefect += `${p.fullRef}\n`;
    });

    productsWithDefect += "-------------------------\n";
    productsWithDefect += "FAILED NO IMAGES\n";
    // console.log("-------------------------------------------------------------------------");
    _.each(productsMissingImages, p => {
        // console.log(p.fullRef);
        productsWithDefect += `${p.fullRef}\n`;
    });

    // console.log(" ----------------------------------------");
    // console.log("Products without PRICES: " + productsMissingPrices.length);
    // console.log("Products without IMAGES: " + productsMissingImages.length);
    // console.log(" ----------------------------------------");

    // fs.appendFileSync(outputIncompleteFile, productsWithDefect);
}
