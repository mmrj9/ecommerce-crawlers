'use strict';

const request = require('request');
const cheerio = require('cheerio');
const URL = require('url-parse');
const _ = require('lodash');
const fs = require('fs');
const readline = require('readline');
const he = require('he');
const MagentoExporter = require('./MagentoExporter');

const BASE_URL = "http://scalpers.es/index.php/";

let productLinks = [];

const magentoExporter = new MagentoExporter("Scalpers", "scalpers", "Default")
const outputCSVFinal = 'output/importScalpers.csv';

//modes
//startCrawlerSite()
get100RandomProducts()

function get100RandomProducts() {
  console.log("--> Get 100 Random Products");

  let counter = 0
  let lines = []
  const rl = readline.createInterface({
     input: fs.createReadStream('output/importScalpers.csv')
    });

    rl.on('line', function (line, x) {
      if (counter == 0 ){
        fs.appendFileSync('output/importScalpers_100_Random.csv', `${line}\n`);
      }
      counter ++;
      lines.push(line);
      console.log('Line from file:', counter);
      // fs.appendFileSync('output/importScalpers_100_Random.csv', line);
    });

    rl.on('close', function(){

      let indexesUses = []
      while (indexesUses.length < 100) {
        const index = Math.floor(Math.random() * lines.length) + 0;
        if (!_.includes(indexesUses, index)){
          indexesUses.push(index)
          fs.appendFileSync('output/importScalpers_100_Random.csv', `${lines[index]}\n`);
        }
      }

      console.log("RL close");
    });
    // rl.close();
}

function getCategoriesLinks(callback) {
  let categoriesLinks = []

  request(BASE_URL, function(error, response, body) {
      // Check status code (200 is HTTP OK)
      if (error || response.statusCode !== 200) {
          console.log(error);
          return;
      }

      let $ = cheerio.load(body, { decodeEntities: false });
      let categoriesA = $(".submenu a.ts") || [];

      if (categoriesA.length > 0 ){
        _.each(categoriesA, function(c){
          c = $(c);
          categoriesLinks.push({name : c.text(), url : c.attr("href")});
        })
      }

      if (callback) {
        callback(categoriesLinks)
      }
  });
}

function startCrawlerSite(){
  fs.appendFileSync(outputCSVFinal, magentoExporter.getMagentoHeaderLine());
  getCategoriesLinks(function(categoriesLinks){

      let count = 0;
      // categoriesLinks = [categoriesLinks[0]]

      _.each(categoriesLinks, function(c){
          request(c.url, function(error, response, body) {
              // Check status code (200 is HTTP OK)
              if (error || response.statusCode !== 200) {
                  console.log(error);
                  return;
              }

              let $ = cheerio.load(body, { decodeEntities: false });
              let products = $(".single2-product-box > a") || [];
              let productsUrl = [];
              if (products.length > 0 ){
                _.each(products, function(p){
                  p= $(p);
                  const plink = p.attr("href");
                  productLinks.push(plink);
                });
              }
              count++;
              if(count === categoriesLinks.length) {
                let fn = function(){
                  console.log("PRODUCTS COUNT " + productLinks.length);

                  crawlProducts()
                }
                fn()
              }
          });
      });
  })
}

function crawlProducts() {
    if (productLinks.length == 0) {
      return
    }else {
      var nextPage = productLinks.pop();


      visitProductDetails(nextPage, crawlProducts);
    }
}


// Get Product Details
function visitProductDetails(url, callback) {

    console.log("Visiting page details " + url);
    // get product details page
    request(url, function(error, response, body) {
        // Check status code (200 is HTTP OK)
        if (error || response.statusCode !== 200) {
            console.log(error);
            return;
        }

        let $ = cheerio.load(body, { decodeEntities: false });

        let image = $('img.active').attr("src");

        let sku = $(".center-copy h1 span").text().trim();
        if (sku) {
            sku = he.decode(sku)
        }
        let name =$('.center-copy h1').text().trim();
        name = name.replace(sku, "").trim();
        if (name) {
            name = he.decode(name)
        }
        let price = $(".price").text().split("€")[0].trim()
        if (price) {
            price = he.decode(price)
        }
        let description = $('.short-description').text().trim();
        if (description) {
            description = he.decode(description)
        }
        let categories = $("li[itemprop='itemListElement'] a");
        let category = $(categories[categories.length - 2]).text().trim()
        if (category) {
            category = he.decode(category)
        }
        let brand = "Scalpers";
        if (brand) {
            brand = he.decode(brand)
        }

        sku = magentoExporter.cleanSku(sku);

        // writing product details do file
        if (price !== '0') {
            console.log(`Image : ${image}`);
            download(image, `./images/scalpers/${sku}.jpg`, function(success) {
                if (success) {
                    let productLine = magentoExporter.getMagentoLine(sku, category, name, price, JSON.stringify(description), `${sku}.jpg`, brand);
                    fs.appendFileSync(outputCSVFinal, productLine);
                }else {
                    console.log("Cannot get image for product " + name);
                }
            });
        }

        callback();

    });
}

// Download Images
let download = function(uri, filename, callback) {
    request.head(uri, function(err, res, body) {
        if (res && res.headers['content-length'] != "0") {
            request(uri).pipe(fs.createWriteStream(filename)).on('close', function(){
                callback(true);
            });
        }else {
            callback(false);
        }
    });
};
