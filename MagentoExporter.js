'use strict'


const _ = require('lodash');

let sku = "";
let store_view_code = "";
let attribute_set_code = "";
let product_type = "simple";
let categories = "";
let product_websites = "base";
let name = "";
let description = "";
let short_description = "";
let weight = "1.0";
let product_online = "1";
let tax_class_name = "None";
let visibility = "Catalog, Search";
let price = 0.0;
let special_price = '';
let special_price_from_date = '';
let special_price_to_date = '';
let url_key = "";
let meta_title = "";
let meta_keywords = "";
let meta_description = "";
let base_image = "";
let base_image_label = "";
let small_image = "";
let small_image_label = "";
let thumbnail_image = "";
let thumbnail_image_label = "";
let created_at = "";
let updated_at = "";
let new_from_date = "";
let new_to_date = "";
let display_product_options_in = "Block after Info Column";
let map_price = "";
let msrp_price = "";
let map_enabled = "";
let gift_message_available = "";
let custom_design = "";
let custom_design_from = "";
let custom_design_to = "";
let custom_layout_update = "";
let page_layout = "";
let product_options_container = "";
let msrp_display_actual_price_type = "Use config";
let country_of_manufacture = "";
let additional_attributes = "";
let qty = 99;
let out_of_stock_qty = -1;
let use_config_min_qty = 1;
let is_qty_decimal = 0;
let allow_backorders = 0;
let use_config_backorders = 1;
let min_cart_qty = 1.0;
let use_config_min_sale_qty = 1;
let max_cart_qty = 1000;
let use_config_max_sale_qty = 1;
let is_in_stock = 1;
let notify_on_stock_below = 1.0;
let use_config_notify_stock_qty = 1;
let manage_stock = 1;
let use_config_manage_stock = 1;
let use_config_qty_increments = 1;
let qty_increments = 1;
let use_config_enable_qty_inc = 0;
let enable_qty_increments = 0;
let is_decimal_divided = 0;
let website_id = 1;
let deferred_stock_update = 0;
let use_config_deferred_stock_update = 1;
let related_skus = "";
let related_position = "";
let crosssell_skus = "";
let upsell_skus = "";
let additional_images = "";
let additional_image_labels = "";
let hide_from_product_page = "";
let custom_options = "";
let bundle_price_type = "";
let bundle_sku_type = "";
let bundle_price_view = "";
let bundle_weight_type = "";
let bundle_values = "";
let associated_skus = "";
let configurable_variations = "";

let root_category = "";

/*
 * Entity that represents an application.
 */
class MagentoExporter {

    constructor(rootCategory, storeView, attributeSet) {
        root_category = rootCategory;
        store_view_code = storeView;
        attribute_set_code = attributeSet;
    }
    getMagentoHeaderLine() {
        return "sku;store_view_code;attribute_set_code;product_type;categories;product_websites;name;description;short_description;weight;product_online;tax_class_name;visibility;price;special_price;special_price_from_date;special_price_to_date;url_key;meta_title;meta_keywords;meta_description;base_image;base_image_label;small_image;small_image_label;thumbnail_image;thumbnail_image_label;created_at;updated_at;new_from_date;new_to_date;display_product_options_in;map_price;msrp_price;map_enabled;gift_message_available;custom_design;custom_design_from;custom_design_to;custom_layout_update;page_layout;product_options_container;msrp_display_actual_price_type;country_of_manufacture;additional_attributes;qty;out_of_stock_qty;use_config_min_qty;is_qty_decimal;allow_backorders;use_config_backorders;min_cart_qty;use_config_min_sale_qty;max_cart_qty;use_config_max_sale_qty;is_in_stock;notify_on_stock_below;use_config_notify_stock_qty;manage_stock;use_config_manage_stock;use_config_qty_increments;qty_increments;use_config_enable_qty_inc;enable_qty_increments;is_decimal_divided;website_id;deferred_stock_update;use_config_deferred_stock_update;related_skus;related_position;crosssell_skus;upsell_skus;additional_images;additional_image_labels;hide_from_product_page;custom_options;bundle_price_type;bundle_sku_type;bundle_price_view;bundle_weight_type;bundle_values;associated_skus;configurable_variations\n";
    }
    
    getMagentoLine(type, visibility, skuLine, categoryLine, nameLine, priceLine, promoPrice, descriptionLine, imageLine, small_image, additional_images, additional_attributes, configurable_variations, related_products) {
        product_type = type;
        sku = skuLine;

        let cats = categoryLine.split(",");
        categories = "";
        _.each(cats, function(c){
            categories = `${categories}${root_category}/${c},`
        })
        categories = categories.substr(0, categories.length - 1);

        name = nameLine;
        price = priceLine;
        special_price = promoPrice;
        description = descriptionLine;
        base_image = imageLine;
        small_image = small_image;
        url_key = skuLine;
        thumbnail_image = small_image;
        additional_attributes = additional_attributes;
        configurable_variations = configurable_variations;
        additional_images = additional_images;
        visibility = visibility;
        related_skus = related_products || "";
        related_position = "";

        if (!_.isEmpty(related_skus)){
            let rSkusSize = related_skus.split(",").length;

            _(rSkusSize).times(function(n){
                let i = n + 1;
                if (i == rSkusSize){
                    related_position += `${i}`;    
                }else {
                    related_position += `${i},`;
                }
                
            });    
        }
        

        return `${sku};${store_view_code};${attribute_set_code};${product_type};${categories};${product_websites};${name};${description};${short_description};${weight};${product_online};${tax_class_name};${visibility};${price};${special_price};${special_price_from_date};${special_price_to_date};${url_key};${meta_title};${meta_keywords};${meta_description};${base_image};${base_image_label};${small_image};${small_image_label};${thumbnail_image};${thumbnail_image_label};${created_at};${updated_at};${new_from_date};${new_to_date};${display_product_options_in};${map_price};${msrp_price};${map_enabled};${gift_message_available};${custom_design};${custom_design_from};${custom_design_to};${custom_layout_update};${page_layout};${product_options_container};${msrp_display_actual_price_type};${country_of_manufacture};${additional_attributes};${qty};${out_of_stock_qty};${use_config_min_qty};${is_qty_decimal};${allow_backorders};${use_config_backorders};${min_cart_qty};${use_config_min_sale_qty};${max_cart_qty};${use_config_max_sale_qty};${is_in_stock};${notify_on_stock_below};${use_config_notify_stock_qty};${manage_stock};${use_config_manage_stock};${use_config_qty_increments};${qty_increments};${use_config_enable_qty_inc};${enable_qty_increments};${is_decimal_divided};${website_id};${deferred_stock_update};${use_config_deferred_stock_update};${related_skus};${related_position};${crosssell_skus};${upsell_skus};${additional_images};${additional_image_labels};${hide_from_product_page};${custom_options};${bundle_price_type};${bundle_sku_type};${bundle_price_view};${bundle_weight_type};${bundle_values};${associated_skus};${configurable_variations}\n`;
    }



    cleanSku(sku){
        sku = sku.replace(/\//g, '-');
        sku = sku.replace(/\./g, '-');
        sku = sku.replace(/\:/g, '-');
        sku = sku.replace(/\ /g, '');
        return sku;
    }
}


module.exports = MagentoExporter;
