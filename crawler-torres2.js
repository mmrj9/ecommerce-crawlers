'use strict';

const request = require('request');
const cheerio = require('cheerio');
const URL = require('url-parse');
const _ = require('lodash');
const fs = require('fs');
const readline = require('readline');
const he = require('he');
const MagentoExporter = require('./MagentoExporter');

const BASE_URL = ["https://www.torres.pt/en/categoria-produto/relogios/", "https://www.torres.pt/en/categoria-produto/joalharia/?pa_marcas=chopard"];

const MAX_PAGES_TO_VISIT = 105;
//const MAX_PAGES_TO_VISIT = 15;
let MAX_PRODUCTS_TO_VISIT = 1000;

let OUTPUT_CSV_FILE = "output/importTorres.csv";
let numPagesVisited = 0;

const MAGENTOEXPORTER = new MagentoExporter("Torres", "default", "Default");

let pagesToVisit = BASE_URL;

console.log(pagesToVisit)


let PRODUCTS_TO_VISIT = []

// STEP 1 ____________________
// Start crawling
console.log('start crawling ...');
getListOfProductDetailsLinks(function(){
    fs.writeFile(OUTPUT_CSV_FILE, '', function(){});
    fs.appendFileSync(OUTPUT_CSV_FILE, MAGENTOEXPORTER.getMagentoHeaderLine());

    startCrawlingProductsDetails()
});
function startCrawlingProductsDetails() {
    MAX_PRODUCTS_TO_VISIT = PRODUCTS_TO_VISIT.length;
    console.log(PRODUCTS_TO_VISIT.length)
    crawlProductLinks();
}


function getListOfProductDetailsLinks(callback) {
    if (numPagesVisited >= 1) {
        console.log("Reached max limit of number of pages to visit.");
        callback()
        return;
    }
    var nextPage = pagesToVisit.pop();

    visitPage(nextPage, getListOfProductDetailsLinks, callback);
}


// Get Product Links
function visitPage(url, callback, callback2) {

    console.log("Visiting page " + url);
    numPagesVisited++;

    //request(`${baseURL}/en/shop/page/${i}/`, function(error, response, body) {
    request(url, function(error, response, body) {

        // Check status code (200 is HTTP OK)
        console.log("Status code: " + response.statusCode);
        if (error || response.statusCode !== 200) {
            callback(callback2);
            return;
        }

        // Parse the document body
        let $ = cheerio.load(body, { decodeEntities: false });

        // get absolute url for each product in current page
        let aaas = $(".wrapper-top-product").children("a[href^='http']");

        aaas.each(function(i, elem) {
            let link = $(this).attr('href');

            if(!_.includes(PRODUCTS_TO_VISIT, link)){
                PRODUCTS_TO_VISIT.push(link);
            }
        });

        callback(callback2);

    });
}


function crawlProductLinks() {
    if (numPagesVisited >= MAX_PRODUCTS_TO_VISIT) {
        console.log("Reached max limit of products to visit.");
        return;
    }
    var nextPage = PRODUCTS_TO_VISIT.pop();

    visitProductDetails(nextPage, crawlProductLinks);
}

// Get Product Details
function visitProductDetails(url, callback) {

    console.log("Visiting page details " + url);
    numPagesVisited++;

    // get product details page
    request(url, function(error, response, body) {

        // Check status code (200 is HTTP OK)
        if (error || response.statusCode !== 200) {
            console.log("ERROR");
            console.log(error);
            return;
        }

        // console.log(body);
        let $ = cheerio.load(body, { decodeEntities: false });
        let baseImage = "";
        let additional_images = "";
        let additional_attributes = "";
        let related_products = "";
        let images = $("ul.list-thumbnails > li > a img") || [];
        let allImagesLinks = [];

        let sku = $(".product_meta [itemprop='sku']").text().trim();
        if (sku) {
            sku = he.decode(sku)
        }

        if (images && images.length > 0) {
            baseImage = ($(images.get(0)).attr("src") || "").trim();

            _.each(images, function(image, index){
                const src = $(image).attr("src").trim();

                allImagesLinks.push(src)

                if (additional_images.length > 0) 
                    additional_images = `${additional_images}, ${sku}-${index}.jpg`
                else 
                    additional_images = `${sku}-${index}.jpg`
            })
        }else {
            baseImage = "";
        }

        
        let name =$('.wrapp-title-product').children('.product_title').text().trim();
        if (name) {
            name = he.decode(name)
        }
        let price = $("div.woo-summary > p.price > span.amount").text();
        if (price) {
            price = he.decode(price.substr(price, price.length - 1))
            price = price.replace("&euro", "")
            price = price.trim()
        }
        let description = $($("div.detail-product div.wrapper-detail-product-info > div")[0]).text().trim();
        if (description) {
            description = he.decode(description)
        }
        let category = $(".product_meta  .posted_in a").text().trim();
        if (category) {
            category = he.decode(category)
        }
        let brand = $(".product_meta  .wb-posted_in a").text().trim();
        if (brand) {
            brand = he.decode(brand)
        }


        sku = MAGENTOEXPORTER.cleanSku(sku);

        if(!baseImage || !category || !price || !description || !sku) {
            callback()
            return
        }

        console.log(`SKU ${sku}`)
        console.log(`NAME ${name}`)
        console.log(`DESCRIPTION ${description}`)
        console.log(`PRICE ${price}`)
        console.log(`CATEGORY ${category}`)
        console.log(`IMAGE ${baseImage}`)
        console.log(`ADDITIONAL_IMAGE ${additional_images}`)
        console.log(`BRAND ${brand}`)

        // writing product details do file
        console.log(`Image : ${baseImage}`);

        let productLine = MAGENTOEXPORTER.getMagentoLine("simple", "Not Visible Individually", sku, category, name, price, "", description, baseImage, baseImage, additional_images, additional_attributes, "", related_products);
        fs.appendFileSync(OUTPUT_CSV_FILE, productLine);    

        _.each(allImagesLinks, function(imgSrc, index){
            download(imgSrc, `./images/torres/${sku}-${index}.jpg`, function(success) {
                if (success) {
                    
                }else {
                    console.log("Cannot get image for product " + name);
                }
            });
        })
        

        callback();

    });
}

// Download Images
let download = function(uri, filename, callback) {
    request.head(uri, function(err, res, body) {
        if (res && res.headers['content-length'] != "0") {
            request(uri).pipe(fs.createWriteStream(filename)).on('close', function(){
                callback(true);
            });
        }else {
            callback(false);
        }
    });
};
