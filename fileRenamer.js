let fs = require('fs');
let gm = require('gm');//.subClass({imageMagick: true});

// let path = '/Users/xarl3z/tmp/zilian_images';
// let newPath = '/Users/xarl3z/tmp/zilian_images_fixed';

// FOLDERS must exists before this strict runs
let path = '/Users/dmsilva/Desktop/zilian_images/original_images';
// let finalPath = '/Users/dmsilva/Desktop/zilian_images/zs_final_small';
let finalPath = '/Users/dmsilva/Desktop/zilian_images/zs_final';

fs.readdir(path, function(err, items) {
   iterateItem(items)
});

function iterateItem(items){
  if (items.length <= 0) {
    console.log("all items iterated.\n going minify");

    return
  }
  let name = items.pop();

  console.log("filename original: " + name);
  // // let newName = name.replace("(", "_").replace(")", "").replace("€", "").replace(" ", "").replace(",", ".");
  let newName = name.replace(/\s+/g, '_').replace(/\(+/g, '_').replace(/\)+/g, '_').replace(/\€/g, '_').replace(/\,/g, '.');
  let fileNameComponents = newName.split('.');

  let originFullPathAndName = `${path}/${name}`;
  let largeImageFullPathAndName = `${finalPath}/${newName}`;
  let smallImageFullPathAndName = `${finalPath}/${fileNameComponents[0]}_small.${fileNameComponents[1]}`;

  imageProcess(originFullPathAndName, largeImageFullPathAndName, smallImageFullPathAndName, items);
  // fs.createReadStream(fullPathAndName).pipe(fs.createWriteStream(newFullPathAndName)).on('finish', function() {
  //     console.log("refactored name: " + name + " to " + newName);
  //       imageProcess(newPath, newName, items);
  // });
}

function imageProcess(originFullPathAndName, largeImageFullPathAndName, smallImageFullPathAndName, items) {

    console.log('trying read file: ' + originFullPathAndName);

    gm(originFullPathAndName).size( (err, value) => {
        if(err) console.log("ERROR: " + JSON.stringify(err));
        else {

            // console.log(JSON.stringify(value));

            //optimize native image
            gm(originFullPathAndName).resize(1024).write(largeImageFullPathAndName, function(err){
                if (err) return console.dir(arguments);
                // console.log(this.outname + " created  ::  " + arguments[3]);
                console.log("Terminate 1024 image save!");
                //generate thumbnail
                gm(originFullPathAndName).resize(300).write(smallImageFullPathAndName, function(err){
                    if (err) return console.dir(arguments);
                    // console.log(this.outname + " created  ::  " + arguments[3]);

                    console.log("Terminate SMALL image save!");

                    console.log("going iterate new item. "+items.length);
                    iterateItem(items);
                });
            });
        }
    });
}
console.log("will exit");
